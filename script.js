// 1. box - to be blurred when you click on this box and not if you click again
/*
var isBlurred = false; //state
var boxOne = document.getElementById("element-one"); //choose element
// choose event
boxOne.onclick = function() {
    blurred();
}
// event function
function blurred() {
    isBlurred = !isBlurred; //state change
    if (isBlurred) {
        boxOne.classList.add("blurred");
    } else {
        boxOne.classList.remove("blurred");
    }
}
*/

var boxOne = document.getElementById("element-one");
boxOne.onclick = function() {
    blurred();
}
function blurred() {
    boxOne.classList.toggle("blurred");
}
//---------------------------------------------------------------------------------------------------------

// 2. box - to be red when you hover on the mouse, and not if you move out the mouse

/* var boxTwo = document.getElementById("element-two");
boxTwo.onmouseover = function() {
    boxTwo.style.backgroundColor = "red";
}
boxTwo.onmouseout = function() {
    boxTwo.style.backgroundColor = "";
}*/

var boxTwo = document.getElementById("element-two");
boxTwo.onmouseover = function() {
    boxTwo.classList.add("beRed");
}
boxTwo.onmouseout = function() {
    boxTwo.classList.remove("beRed");
}

// 3.box - if you double click on this element, the code will result a random number for 1 to 20, and this choosen number will be in text of this box

var boxThree = document.getElementById("element-three");
boxThree.ondblclick = function() {
  boxThree.firstElementChild.innerHTML = RandomNumberGenerator(1, 20);
}
function RandomNumberGenerator(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

// 4. box - if you click on this element, it will disappear and after 1 second it will appear
/*
var boxFour = document.getElementById("element-four");

boxFour.onclick = function() {
    boxFour.style.display = "none";
    setTimeout(function() {
        boxFour.style.display = "block";
    }, 1000);
}*/

var boxFour = document.getElementById("element-four");
boxFour.onclick = function() {
    boxFour.classList.add("appearance");
    setTimeout(function() {
        boxFour.classList.remove("appearance");
    }, 1000);
}

// 5.box - if you click this element, all elements will be round shape

/*
var boxFive = document.getElementById("element-five");
var beRound = false;
const elementList = document.querySelectorAll(".shape");
boxFive.onclick = function() {
    beRound = !beRound;
    if (beRound) {
        for (let idx = 0; idx < elementList.length; idx++) {
            elementList[idx].classList.add("beRound");
        }
    } else {
        for (let idx = 0; idx < elementList.length; idx++) {
            elementList[idx].classList.remove("beRound");
        }
    }
}
 */
/*
var boxFive = document.getElementById("element-five");
const elementList = document.querySelectorAll(".shape");
boxFive.onclick = function() {
    for (let idx = 0; idx < elementList.length; idx++) {
        elementList[idx].classList.toggle("beRound");
    }
}*/
var boxFive = document.getElementById("element-five");
var elements = document.querySelectorAll(".shape");
boxFive.onclick = function() {
    for (var element of elements) {
        element.classList.toggle("beRound");
    }
}

//----------------------------------------------------------------------------------------
// 6. box - change data of the box with a form submit button

/* 
element - form element
event - onsubmit
   
*/
var boxSix = document.getElementById("element-six");
boxSix.onsubmit = function (event) {
    event.preventDefault();
    boxSix.firstElementChild.innerHTML = event.target.elements.boxNumber.value;
}

//--------------------------------------------------------------------------------------------
// 7.box - after keypress event, change data of the box

var boxSeven = document.getElementById("element-seven");
boxSeven.onkeypress = function (event) {
    boxSeven.firstElementChild.innerHTML = event.key;
}

//----------------------------------------------------------------------------------------------
// 8.box - when mouse move, write in the box the coordinates of the mouse movement

/*
element - box
event - mouse movement
*/

var boxEight = document.getElementById("element-eight");
document.onmousemove = function (event) {
    var coordinates = event.clientX + "," + event.clientY;
    boxEight.firstElementChild.innerHTML = coordinates;
}

// 9.box - after submit event, change the content of box with a new value, we have a start value, a number and an operation. New value is start value nad operation and number.
// For example: start value = 4, operation = +, number = 2, so the new value is 4 + 2 = 6

var boxNine = document.getElementById("element-nine");
var actualState = boxNine.firstElementChild.innerHTML;
boxNine.onsubmit = function (event) {
    event.preventDefault();
    var operator = event.target.elements.operation.value;
    var userNumber = event.target.elements.boxNumber.value;
    var finalValue;
    switch (operator) {
        case "+":
            Number("+");
            finalValue = eval(actualState + operator + userNumber);
            break;
        case "-":
            finalValue = eval(actualState + operator + userNumber);
            break;
        case "*":
            finalValue = eval(actualState + operator + userNumber);
            break;
        case "/":
            finalValue = eval(actualState + operator + userNumber);
            break;
    }
    boxNine.firstElementChild.innerHTML = finalValue;
    actualState = finalValue;
}


